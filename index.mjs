// import glob from "glob";
import JestHasteMap from "jest-haste-map";
import { cpus } from "os";
import { dirname, join, relative } from "path";
import { fileURLToPath } from "url";
import { Worker } from "jest-worker";
import chalk from "chalk";

// const testFiles = glob.sync("**/*.test.js");

// get the root path to this project
const root = dirname(fileURLToPath(import.meta.url));

const hasteMapOptions = {
  extensions: ["js"], // only crawl .js files
  maxWorkers: cpus().length, // parallel across all available CPUs
  name: "js-test-framework", // used for caching?
  platforms: [],
  rootDir: root,
  roots: [root], // can be used to search only for a subset of files within `root`
};
const hasteMap = new JestHasteMap.default(hasteMapOptions);
await hasteMap.setupCachePath(hasteMapOptions);

// build and return an in-memory HasteFS instance
const { hasteFS } = await hasteMap.build();
// a Set
// with test filtering
// run this command to run only one test:
//   `node index.mjs mock.test.js`
const testFiles = hasteFS.matchFilesWithGlob([
  process.argv[2] ? `**/${process.argv[2]}*` : "**/*.test.js",
]);
console.log(testFiles);

// new Worker
const worker = new Worker(join(root, "worker.js"), {
  enableWorkerThreads: true,
});

let hasFailed = false;
await Promise.all(
  Array.from(testFiles).map(async (testFile) => {
    const { success, errorMessage, testResults } = await worker.runTest(
      testFile,
    );
    const status = success
      ? chalk.green.inverse.bold(" PASS ")
      : chalk.red.inverse.bold(" FAIL ");
    console.log(status + " " + chalk.dim(relative(root, testFile)));
    if (!success) {
      hasFailed = true;
      if (testResults) {
        testResults
          .filter((result) => result.errors.length)
          .forEach((result) =>
            console.log(
              result.testPath.slice(1).join(" ") + "\n" + result.errors[0],
            ),
          );
      } else if (errorMessage) {
        console.log("  " + errorMessage);
      }
    }
  }),
);

worker.end();

if (hasFailed) {
  console.log(
    "\n" + chalk.red.bold("Test run failed, please fix all the failing tests!"),
  );

  // set exit code
  process.exitCode = 1;
}
