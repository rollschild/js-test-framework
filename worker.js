const fs = require("fs");
const expect = require("expect").default;
const mock = require("jest-mock");
const { describe, it, run, resetState, test } = require("jest-circus");
const vm = require("vm");
const NodeEnvironment = require("jest-environment-node").default;
const { dirname, join, basename } = require("path");

exports.runTest = async function (testFile) {
  // const code = await fs.promises.readFile(testFile, "utf8");
  const testResult = {
    success: false,
    errorMessage: undefined,
  };
  // const expect = (received) => ({
  //   toBe: (expected) => {
  //     if (expected !== received) {
  //       throw new Error(`Expected: ${expected}; received: ${received}!`);
  //     }
  //     return true;
  //   },
  // });

  // let currTestSuiteName;
  try {
    // Manually enable `describe` and `it` in local scope
    // const describeFns = [];
    // let currentDescribeFn;
    // const describe = (name, fn) => describeFns.push([name, fn]);
    // const it = (name, fn) => currentDescribeFn.push([name, fn]);

    resetState();

    // `eval` has access to scope around it
    // Function(code)();
    // eval(code);

    // Use the vm module to sandbox code
    // the `context` variable acts as the global variable inside the vm
    // const context = { describe, it, expect, mock };
    // vm.createContext(context);

    let environment;
    const customRequire = (filename) => {
      const code = fs.readFileSync(join(dirname(testFile), filename), "utf8");
      // inject `require` as a variable here
      const moduleFactory = vm.runInContext(
        `(function(module, require) {${code}})`,
        environment.getVmContext(),
      );
      const module = { export: {} };

      // Run the sandboxed function with our module object
      moduleFactory(module, customRequire);
      // we are now able to require multiple modules
      // and each of them get their own "module" scope
      return module.exports;
      // return vm.runInContext(
      //   "const module = {exports: {}};\n" + code + ";module.exports;",
      //   environment.getVmContext(),
      // );
    };

    // NodeEnvironment provides an Node.js-like environment for tests
    environment = new NodeEnvironment({
      projectConfig: {
        testEnvironmentOptions: {
          describe,
          it,
          expect,
          mock,
        },
      },
    });
    customRequire(basename(testFile));

    // vm.runInContext(code, environment.getVmContext());

    // run jest-circus
    const { testResults } = await run();
    testResult.testResults = testResults;
    testResult.success = testResults.every((result) => !result.errors.length);

    // Manually run each `describe` and `it`
    // for (const [testSuiteName, describeFn] of describeFns) {
    //   currentDescribeFn = [];
    //   currTestSuiteName = testSuiteName;
    //   describeFn();
    //
    //   currentDescribeFn.forEach(([name, fn]) => {
    //     currTestSuiteName += " " + name;
    //     fn();
    //   });
    // }
    //
    // testResult.success = true;
  } catch (e) {
    // testResult.errorMessage = currTestSuiteName + ": " + e.message;
    testResult.errorMessage = e.message;
  }

  return testResult;

  // return testFile + ":\n" + code;
  // return `worker id: ${process.env.JEST_WORKER_ID}\nfile: ${testFile}:\n${code}`;
};
