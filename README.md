# A testing framework for JavaScript

## How to run

- To run all tests within the `tests/` folder:

```console
$ node index.mjs
```

- To run a single test file

```console
$ node index.mjs <test-file-name-here>
```

## Implementation details

### Steps

1. search for test files
2. run all tests in parallel

### Efficiently search for test files

- the first step
- we use `jest-haste-map`
  - crawls the entire project, extracts dependencies and analyzes files in parallel across worker processes
  - keeps a cache of the file system in memory and on disk so that file related operations are fast
  - minimal amount of work required when files change
  - watches the file system for changes - useful for building interactive tools

### Run all tests in parallel

- we use worker threads to utilize all available CPUs

### Isolate tests from each other

- Use the `vm` module provided by Node.js
